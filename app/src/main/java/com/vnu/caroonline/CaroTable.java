package com.vnu.caroonline;

public class CaroTable {

    // Các hằng trạng thái của các ô trên bàn cờ
    public static final int STATE_NONE = 0;
    public static final int STATE_X = 1;
    public static final int STATE_O = 2;

    // Quy định số quân cờ liên tiếp để thắng
    public static final int WIN_SIZE = 5;

    // Kích thước bàn cờ
    private int size;
    // Ma trận bàn cờ
    private int[][] table;

    // Quân cờ tiếp theo được đánh, mặc định là X
    private int nextStatus = STATE_X;

    /**
     * Khởi tạo một bàn cờ mới
     *
     * @param size kích thước bàn cờ
     */
    public CaroTable(int size) {
        this.size = size;
        table = new int[size][size];
    }

    public CaroTable(int[][] data) {
        this.table = data;
        this.size = table.length;
    }

    // Trả về kích thước bàn cờ
    public int getTableSize() {
        return size;
    }

    // Trả về số ô của bàn cờ
    public int getSize() {
        return size * size;
    }

    // Trả về quân cờ tiếp theo trên bàn cờ X | O
    public int getNextState() {
        return nextStatus;
    }

    // Xóa bàn cờ
    public void clear() {
        table = new int[size][size];
        //Trả về trạng thái người đánh trước là X
        nextStatus = STATE_X;
    }

    /**
     * Đánh một nước cờ vào bàn cờ
     *
     * @param x tọa độ x
     * @param y tọa độ y
     */
    public void put(int x, int y) {
        table[x][y] = nextStatus;

        // Chuyển sang nước tiếp theo
        if (nextStatus == STATE_X) {
            nextStatus = STATE_O;
        } else {
            nextStatus = STATE_X;
        }
    }

    /**
     * Kiểm tra xem đã thắng chưa?
     *
     * @param x          tọa độ dánh lúc trước
     * @param y          tọa độ đánh lúc trước
     */
    public boolean checkWin(int x, int y) {

        // Xác định quân cờ vừa đánh
        int checkState = table[x][y];
        // Biến đếm số quân cờ
        int count = 0;

        // Kiểm tra trên hàng
        for (int i = 0; i < size; i++) {
            if (table[x][i] != checkState) {
                count = 0;
                continue;
            } else {
                count++;
                if (count == WIN_SIZE) {
                    return true;
                }
            }
        }

        // Kiểm tra trên cột
        for (int i = 0; i < size; i++) {
            if (table[i][y] != checkState) {
                count = 0;
                continue;
            } else {
                count++;
                if (count == WIN_SIZE) {
                    return true;
                }
            }
        }

        // Kiểm tra trên đường chéo chính
        int _x = x + y;
        count = 0;
        for (int i = 0; i < size; i++) {
            if ((_x - i) >= size || (_x - i) < 0) {
                continue;
            }

            if (table[_x - i][i] != checkState) {
                count = 0;
                continue;
            } else {
                count++;
                if (count == WIN_SIZE) {
                    return true;
                }
            }
        }

        // Kiểm tra trên đường chéo phụ
        _x = x - y;
        count = 0;
        for (int i = 0; i < size; i++) {
            if ((_x + i) >= size || (_x + i) < 0) {
                continue;
            }

            if (table[_x + i][i] != checkState) {
                count = 0;
                continue;
            } else {
                count++;
                if (count == WIN_SIZE) {
                    return true;
                }
            }
        }

        return false;
    }

    // Trả về quân cờ tại vị trí [row][col]
    public int getStateAt(int row, int col) {
        return table[row][col];
    }
}
