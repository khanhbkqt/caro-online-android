package com.vnu.caroonline;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by hopho on 08/04/2016.
 */
public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {
    private UserAdapterListener mListener;
    private Context mContext;
    private List<User> mUserList;

    public UserAdapter(List<User> mUserList) {
        this.mUserList = mUserList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_user, parent, false);
        this.mContext = parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final User user = mUserList.get(position);

        holder.tvUserName.setText(user.getUsername());
        holder.btnFight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    if (user.isFree()) {
                        mListener.onFight(position);
                        user.setFighting(true);
                        notifyDataSetChanged();
                    } else {
                        Toast.makeText(mContext, "Sorry, this user is busy", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        if (user.isFighting()) {
            holder.btnFight.setText("Waiting ...");
        } else {
            holder.btnFight.setText("Fight");
        }
        if (user.isFree()) {
            holder.vStatus.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_status_free));
        } else {
            holder.vStatus.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_status_busy));
        }
    }

    @Override
    public int getItemCount() {
        return (mUserList != null) ? mUserList.size() : 0;
    }

    public void setUserAdapterListener(UserAdapterListener listener) {
        this.mListener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvUserName;
        private Button btnFight;
        private View vStatus;

        public ViewHolder(View itemView) {
            super(itemView);

            tvUserName = (TextView) itemView.findViewById(R.id.tv_user_name);
            btnFight = (Button) itemView.findViewById(R.id.btn_fight);
            vStatus = itemView.findViewById(R.id.v_status);
        }
    }

    public interface UserAdapterListener {
        void onFight(int position);
    }
}
