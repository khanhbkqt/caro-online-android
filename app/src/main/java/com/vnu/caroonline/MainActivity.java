package com.vnu.caroonline;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements CaroAdapter.CaroAdapterListener, UserAdapter.UserAdapterListener {

    // Socket kết nối với server
    private Socket socket;

    private PrintWriter output;
    private BufferedReader input;

    private CaroTable mCaroTable;

    // Biến đánh dấu xem hiện tại có được đánh hay ko, mặc định là ko
    private boolean canMove = false;
    private String myMark;
    private String myName;
    private String opponentName;

    private RecyclerView mRecyclerView;
    private CaroAdapter mCaroAdapter;

    private TextView tvPlayer1, tvPlayer2, tvPlayerMark1, tvPlayerMark2;
    private View vPlayer1, vPlayer2;

    // Thanh Progress lúc kết nối server
    private ProgressDialog mProgressDialog;

    //Danh sách tên các User
    private List<User> mUserList;
    private UserAdapter mUserAdapter;

    // Dialog hỏi bạn có muốn đấu lại hay không
    private AlertDialog mFightAgainDialog;

    /**
     * Hàm này nó tự gọi khi bật chương trình lên
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Đặt giao diện là tệp activity_main.xml
        setContentView(R.layout.activity_main);

        // Tạo một bàn cờ
        mCaroTable = new CaroTable(20);

        // Khởi tạo các views
        tvPlayer1 = (TextView) findViewById(R.id.tv_player_1);
        tvPlayer2 = (TextView) findViewById(R.id.tv_player_2);
        tvPlayerMark1 = (TextView) findViewById(R.id.tv_timer_1);
        tvPlayerMark2 = (TextView) findViewById(R.id.tv_timer_2);

        vPlayer1 = findViewById(R.id.view_player_1);
        vPlayer2 = findViewById(R.id.view_player_2);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        mUserList = new ArrayList<>();

        connectServer();
    }

    // Hàm này yêu cầu người dùng nhập tên sau đó kết nối đến server
    private void connectServer() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enter your name");

        // Set up the input
        final EditText input = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("Connect", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                myName = input.getText().toString();
                // Bắt đầu chạy luồng GameThread
                new GameThread().execute();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();


    }

    /**
     * Hàm khởi tạo một game mới
     */
    private void newGame() {
        // Xóa bàn cờ
        mCaroTable.clear();
        mCaroAdapter.notifyDataSetChanged();

        // Đánh dấu người chơi hiện tại
        setCurrentPlayer();

        // Cuộn ra giữa bàn cờ
        mRecyclerView.postDelayed(new Runnable() {
            @Override
            public void run() {
                int scrollOffset = Helper.dpToPx(MainActivity.this, 150);

                mRecyclerView.smoothScrollBy(scrollOffset, scrollOffset);

                // Lắng nghe sự kiện bàn cờ
                mCaroAdapter.setListener(MainActivity.this);
            }
        }, 1000);
    }

    /**
     * Đánh dấu người chơi hiện tại
     */
    private void setCurrentPlayer() {

        if (myMark.equals("X")) {
            vPlayer1.setSelected(mCaroTable.getNextState() == CaroTable.STATE_X);
            vPlayer2.setSelected(mCaroTable.getNextState() == CaroTable.STATE_O);
        } else {
            vPlayer1.setSelected(mCaroTable.getNextState() == CaroTable.STATE_O);
            vPlayer2.setSelected(mCaroTable.getNextState() == CaroTable.STATE_X);
        }
    }

    // Đặt quân cờ cho mình
    private void setMyMark(String mark) {

        myMark = mark;

        tvPlayerMark1.setText(mark);

        if (mark.equals("X")) {
            tvPlayerMark2.setText("O");
        } else {
            tvPlayerMark2.setText("X");
        }
        setCurrentPlayer();
    }

    /**
     * Được gọi khi người chơi thực hiện chạm trên 1 ô trống bàn cờ
     *
     * @param row dòng
     * @param col cột
     */
    @Override
    public void onNewMove(int row, int col) {
        if (canMove) {
            move(row, col);
            canMove = false;
            sendMoveToServer(row, col);
        }
    }

    // Hàm này gửi lệnh MOVE lên server khi người chơi đánh cờ
    private void sendMoveToServer(final int row, final int col) {
        new Thread() {
            @Override
            public void run() {
                output.println("MOVE " + (row * mCaroTable.getTableSize() + col));
                output.flush();
            }
        }.start();
    }

    private void move(int row, int col) {
        // Đặt quân cờ lên bàn cờ
        mCaroTable.put(row, col);
        // Yêu cầu adapter vẽ lại view ở ô vừa đánh
        mCaroAdapter.notifyItemChanged(row, col);
        // Đánh dấu lại người chơi hiện tại
        setCurrentPlayer();

        // Kiểm tra xem thắng chưa
        if (mCaroTable.checkWin(row, col)) {
            // Nếu thắng rồi thì vô hiệu hóa nước đi
            mCaroAdapter.setListener(null);
            String win = mCaroTable.getStateAt(row, col) == CaroTable.STATE_O ? "O" : "X";

            // Hiển thị thông báo
            showWinnerMessage(win);
        }
    }

    private void showWinnerMessage(String winner) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Caro Online")
                .setMessage(winner + " has win the game.\n" +
                        "Do you want to play new game?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        sendInvitationTo(opponentName);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        output.println("END_GAME");
                        output.flush();
                        exitGame();
                    }
                });
        mFightAgainDialog = builder.show();
    }

    private void exitGame() {
        for (User user : mUserList) {
            user.setFighting(false);
        }
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mRecyclerView.setAdapter(mUserAdapter);
        mUserAdapter.notifyDataSetChanged();

        clearInformationPreviousGame();
    }

    // Xóa tất cả thông tin game đấu vừa rồi
    private void clearInformationPreviousGame() {
        myMark = null;
        opponentName = null;

        this.tvPlayer2.setText("Player 2");
        this.tvPlayerMark1.setVisibility(View.GONE);
        this.tvPlayerMark2.setVisibility(View.GONE);
        this.vPlayer1.setSelected(false);
        this.vPlayer2.setSelected(false);
    }

    @Override
    public void onFight(int position) {
        User user = mUserList.get(position);
        sendInvitationTo(user.getUsername());
    }

    private void sendInvitationTo(String username) {
        output.println("SEND_INVITATION_TO " + username);
        output.flush();
    }

    // Luồng chạy ngầm của Game, lắng nghe các lệnh từ server trả về
    private class GameThread extends AsyncTask<Void, String, Void> {

        // Hàm chạy ngay trước khi bắt đầu luồng
        @Override
        protected void onPreExecute() {
            // Hiển thị thanh Progress
            mProgressDialog = ProgressDialog.show(MainActivity.this, null, "Connecting to server...");
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                // Connect đến server
                if (socket == null || !socket.isConnected()) {
                    socket = new Socket("192.168.1.10", 12345);

                    output = new PrintWriter(socket.getOutputStream());
                    input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                }

                // Thông báo tên người chơi lên server
                output.println("USERNAME " + myName);
                output.flush();

                // Thông báo cho luồng biết là đã kết nối đc server
                publishProgress("CONNECTED");


                while (socket.isConnected()) {
                    // Đọc lệnh từ server và thông báo xử lí
                    String command = input.readLine();
                    publishProgress(command);
                }

            } catch (IOException e) {
                e.printStackTrace();
                mProgressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Fail to connect to server", Toast.LENGTH_SHORT).show();
            }

            return null;
        }


        // Hàm này xử lí các thông báo từ hàm publishProgress
        @Override
        protected void onProgressUpdate(String... values) {
            // Lấy tham số truyền vào từ hàm publishProgress

            String command = values[0];

            System.out.println("Command = " + command);
            if (command == null)
                return;

            // Đoạn này chắc dễ đọc, a ko cần cụ thể
            if (command.startsWith("CONNECTED")) {
                mProgressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Connected to Server", Toast.LENGTH_SHORT).show();
            } else if (command.startsWith("MESSAGE All players connected")) {
                notifyAllPlayersConnected();
            } else if (command.startsWith("MESSAGE Your move")) {
                notifyFirstMove();
            } else if (command.startsWith("OPPONENT_MOVE")) {
                int location = Integer.parseInt(command.substring(14));
                opponentMove(location);
            } else if (command.startsWith("OPPONENT_QUIT")) {
                notifyOpponentQuit();
            } else if (command.startsWith("PLAYER_LIST")) {
                tvPlayer1.setText(myName);
                showUserList(command);
            } else if (command.startsWith("INVITATION_FROM")) {
                receiveInvitation(command.substring(16));
            } else if (command.startsWith("INVITATION_REJECTED_BY")) {
                String username = command.substring(23);
                Toast.makeText(MainActivity.this, "Your invitation is rejected by " + username
                        , Toast.LENGTH_SHORT).show();
                invitationRejectedBy(username);
            } else if (command.startsWith("START_GAME_WITH")) {
                startGame(command.substring(16));
            } else if (command.startsWith("USERNAME_EXISTED")) {
                mProgressDialog.dismiss();
                String username = command.substring(17);
                enterUserNameAgain(username);
            } else if (command.startsWith("CLIENT_ERROR")) {
                Toast.makeText(MainActivity.this, "There is something wrong on client", Toast.LENGTH_SHORT).show();
            } else if (command.startsWith("SERVER_ERROR")) {
                Toast.makeText(MainActivity.this, "There is something wrong on server", Toast.LENGTH_SHORT).show();
            } else if (command.startsWith("END_GAME_FROM")) {
                notifyEndGameFrom(command.substring(14));
            }


        }
    }

    private void notifyEndGameFrom(String username) {
        if (mFightAgainDialog != null && mFightAgainDialog.isShowing()){
            mFightAgainDialog.dismiss();
        }
        Toast.makeText(this, username + " stopped playing with you", Toast.LENGTH_SHORT).show();
        exitGame();
    }

    private void invitationRejectedBy(String username) {
        if (opponentName!=null && !opponentName.isEmpty() && opponentName.equals(username)){
            // Trong trường hợp đang chơi
            exitGame();
        }else{
            // Trong trường hợp chưa chơi
            for (User user : mUserList) {
                if (user.getUsername().equals(username)) {
                    user.setFighting(false);
                }
            }
            mUserAdapter.notifyDataSetChanged();
        }
    }

    private void enterUserNameAgain(String username) {
        Toast.makeText(this, username + " is existed. Please enter another username!!!", Toast.LENGTH_SHORT).show();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enter your name");

        // Set up the input
        final EditText input = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("Connect", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                myName = input.getText().toString();
                if (socket != null && socket.isConnected()) {
                    output.println("USERNAME " + myName);
                    output.flush();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();
    }

    private void startGame(String opponentName) {
        // Thiết đặt giao diện
        FixedGridLayoutManager layoutManager = new FixedGridLayoutManager();
        layoutManager.setTotalColumnCount(mCaroTable.getTableSize());

        mRecyclerView.setLayoutManager(layoutManager);

        // CaroAdapter là một lớp giúp chuyển từ data ở lớp CaroTable
        // thành giao diện hiển thị trên màn hình
        if (mCaroAdapter == null) {
            mCaroAdapter = new CaroAdapter(this, mCaroTable);
            mCaroAdapter.setListener(this);
        }

        mRecyclerView.setAdapter(mCaroAdapter);

        String[] array = opponentName.split(" ");

        opponentName = "";
        opponentName += array[1];

        tvPlayer2.setText(array[1]);
        tvPlayerMark1.setVisibility(View.VISIBLE);
        tvPlayerMark2.setVisibility(View.VISIBLE);

        if (array[0].equals("X")) {
            //Đối thủ là X thì mình O
            setMyMark("O");
        } else if (array[0].equals("O")) {
            //Đối thủ là O thì mình X
            setMyMark("X");
        }
        newGame();
    }

    private void receiveInvitation(final String from) {
        if (mFightAgainDialog != null && mFightAgainDialog.isShowing()) {
            mFightAgainDialog.dismiss();
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Caro Online")
                .setMessage(from + " want to invite you to his game.\n" +
                        "Do you want to play with him?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        acceptInvitation(from);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        rejectInvitation(from);
                    }
                });
        builder.show();
    }


    private void acceptInvitation(String from) {
        if (opponentName == null || opponentName.isEmpty()) {
            opponentName = "";
            opponentName += from;
        }

        output.println("ACCEPT_INVITATION_FROM " + from);
        output.flush();
    }

    private void rejectInvitation(String from) {
        // Nếu trong trường hợp là lời mời đấu lại
        if (opponentName != null && !opponentName.isEmpty() && opponentName.equals(from)){
            exitGame();
        }
        output.println("REJECT_INVITATION_FROM " + from);
        output.flush();
    }

    private void showUserList(String command) {
        String result = command.substring(12);
        String[] users = result.split(",");
        mUserList.clear();
        for (String item : users) {
            if (!item.isEmpty()) {
                String[] arr = item.split("-");
                mUserList.add(new User(arr[0], false, Boolean.parseBoolean(arr[1])));
            }
        }

        if (mUserAdapter == null) {
            mUserAdapter = new UserAdapter(mUserList);
            mUserAdapter.setUserAdapterListener(this);
            mRecyclerView.setAdapter(mUserAdapter);
        } else {
            mUserAdapter.notifyDataSetChanged();
        }


    }

    private void notifyOpponentQuit() {

    }

    // Gọi khi nhận được lệnh đối thủ đánh cờ
    private void opponentMove(int location) {

        // Tìm số dòng / cột
        int row = location / mCaroTable.getTableSize();
        int col = location % mCaroTable.getTableSize();

        // Đặt quân cờ lên bàn cờ
        move(row, col);
        // Cho phép người chơi đánh cờ
        canMove = true;
    }

    // Đặt tên cho đối thủ
    private void setOpponentName(String opponentName) {
        tvPlayer2.setText(opponentName);
    }

    // Gọi khi nhận được thông báo chơi đầu tiên
    private void notifyFirstMove() {
        // Cho phép người chơi di chuyển
        canMove = true;
        Toast.makeText(this, "You move first", Toast.LENGTH_SHORT).show();
    }

    // Khi nhận được thông báo đã kết nối bàn cờ, tạo game mới để chuẩn bị chơi
    private void notifyAllPlayersConnected() {
        Toast.makeText(this, "Opponent connected!", Toast.LENGTH_SHORT).show();
        newGame();
    }
}
