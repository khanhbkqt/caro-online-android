package com.vnu.caroonline;

/**
 * Created by hopho on 08/04/2016.
 */
public class User {
    private String username;
    private boolean fighting;
    private boolean isFree;

    public User(String username, boolean fighting, boolean free) {
        this.username = username;
        this.fighting = fighting;
        this.isFree = free;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isFighting() {
        return fighting;
    }

    public void setFighting(boolean fighting) {
        this.fighting = fighting;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean free) {
        isFree = free;
    }
}
