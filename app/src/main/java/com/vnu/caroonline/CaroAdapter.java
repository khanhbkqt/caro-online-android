package com.vnu.caroonline;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class CaroAdapter extends RecyclerView.Adapter<CaroAdapter.ViewHolder> {

    private CaroAdapterListener mListener;

    private CaroTable mCaroTable;
    private Drawable iconX, iconO;

    // Khởi tạo adapter
    // Truyền vào một bàn cờ CaroTable
    public CaroAdapter(Context context, CaroTable caroTable) {
        this.mCaroTable = caroTable;
        this.iconX = ContextCompat.getDrawable(context, R.drawable.icon_x);
        this.iconO = ContextCompat.getDrawable(context, R.drawable.icon_o);
    }

    public void setListener(CaroAdapterListener listener) {
        this.mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_caro, parent, false);

        return new CaroAdapter.ViewHolder(view);
    }

    // Được gọi khi vẽ bàn cờ ở ô thứ position
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        // Xác định dòng, cột của ô đó
        final int col = position % mCaroTable.getTableSize();
        final int row = position / mCaroTable.getTableSize();

        // Lấy giá trị của ô đó (X | O | Rỗng)
        int currentState = mCaroTable.getStateAt(row, col);

        // Đặt hình ảnh vào ô, nếu vị trí rỗng thì ko đặt
        if (currentState == CaroTable.STATE_NONE) {
            holder.imageView.setImageBitmap(null);
        } else if (currentState == CaroTable.STATE_X) {
            holder.imageView.setImageDrawable(iconX);
        } else if (currentState == CaroTable.STATE_O) {
            holder.imageView.setImageDrawable(iconO);
        }

        // Nếu vị trí là rỗng thì cho phép đánh lên ô đó bằng cách đặt một listener lên ô này
        if (currentState == CaroTable.STATE_NONE) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onNewMove(row, col);
                    }
                }
            });
        // Ngược lại ta xóa listener
        } else {
            holder.itemView.setOnClickListener(null);
        }
    }

    public void notifyItemChanged(int row, int col) {
        int position = row * mCaroTable.getTableSize() + col;
        notifyItemChanged(position);
    }

    @Override
    public int getItemCount() {
        return mCaroTable == null ? 0 : mCaroTable.getSize();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.image_view);
        }
    }

    // Interface để chuyển sự kiện đánh bàn cờ từ adapter ra chương trình, xử lí ở hàm
    // onNewMove ở MainActivity
    public interface CaroAdapterListener {
        void onNewMove(int row, int col);
    }
}
