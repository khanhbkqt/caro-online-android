package com.vnu.caroonline;

import android.content.Context;

public class Helper {

    // Đổi đơn vị dp trong android ra pixel
    public static int dpToPx(Context context, float dp) {
        return (int) (context.getResources().getDisplayMetrics().density * dp);
    }
}
